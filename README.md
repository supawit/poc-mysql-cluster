# init docker swarm
in this example we use 3 node with docker engine installed.

on node-1 init docker with
```
docker swarm init
```

on node-2 and node-3 use output from node-1 to join docker swarm like
```
docker swarm join --token SWMTKN-1-4impjpx0s50fuzsxq2k10cu866nsb0e08r0oarzlnqrlcx0oy8-8ovsps1s3hr8x8e1294xdxdot 10.110.60.31:2377
```

to comfirm that docker swarm work fine use
```
docker node ls
```

# Create overlay network for mysql-cluster
on node-1
```
docker network create --driver overlay --attachable --subnet 192.168.0.0/24 mysql-cluster
```